/**
 * Images should be stored in the `App/Assets/Images` directory and referenced using variables defined here.
 */

export default {
  user_default: require('App/Assets/Images/user_default.jpg'),
}
