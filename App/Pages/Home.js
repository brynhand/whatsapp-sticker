// import React, { Component } from 'react';
// import {
//   Dimensions,
//   FlatList,
//   Image,
//   Modal,
//   Platform,
//   StyleSheet,
//   TouchableOpacity,
//   View,
// } from 'react-native';
// import { Button, Icon, Text, Spinner } from 'native-base';

// import { connect } from 'react-redux';

// import lang from 'App/Helpers/Languages';
// import AppFrame from 'App/Components/AppFrame';

// import * as Styles from 'App/Styles';
// import * as Hooks from 'App/Helpers/Hooks';

// import RNWhatsAppStickers from "react-native-whatsapp-stickers";
// import RNFetchBlob from 'rn-fetch-blob';
// import fs, { stat } from "react-native-fs";

// const TAG = "HOME: ";
// const { width, height } = Dimensions.get('window');

// class Home extends Component {
//   static navigationOptions = () => ({ headerShown: false });

//   constructor(props) {
//     super(props);

//     this.state = {
//       folderSticker: [],
//       stickerDetails: [],
//       modalTitle: '',
//       modalShow: false,
//       isLoading: false,
//       refreshing: false,
//     }
//   }

//   componentDidMount() {
//     // this.fsReadAssets();
//     this.readStickersJSON();
//   }

//   readStickersJSON() {
//     fs.readFileAssets('contents.json').then(payload => {
//       this.setState({ folderSticker: JSON.parse(payload).sticker_packs });
//       Hooks.consoleLog(TAG + 'data contents.json', this.state.folderSticker);
//     });
//   }

//   RNFetchBlobReadAssets() {
//     var ANDROID_ASSETS_FOLDER = RNFetchBlob.fs.asset('assets');
//     Hooks.consoleLog(TAG + "Files list in ANDROID_ASSETS_FOLDER = ", ANDROID_ASSETS_FOLDER);
//     Hooks.consoleLog(TAG + "Files list in ANDROID_ASSETS_FOLDER = ", RNFetchBlob.fs.ls(ANDROID_ASSETS_FOLDER));

//     RNFetchBlob.fs.ls(ANDROID_ASSETS_FOLDER).then((files) => {
//       Hooks.consoleLog(TAG, files.length);
//       Hooks.consoleLog(TAG, files);
//       Hooks.consoleLog(TAG, files[0]);
//     });
//   }

//   fsReadAssets() {
//     let folderSticker = [];

//     fs.readDirAssets('').then(payload => {
//       Hooks.consoleLog(TAG + "payload: ", payload);

//       for (let item of payload) {
//         if (item.isDirectory()) {
//           if (item.name.match(/STICKER_/g) != null) {
//             fs.readDirAssets(item.name).then(subpayload => {
//               Hooks.consoleLog(TAG + "payload subfolder: ", subpayload);

//               folderSticker.push({
//                 name: item.name,
//                 files: subpayload
//               });
//             }).catch(e => {
//               Hooks.console(TAG + "error read subfolder: ", e);
//               return false;
//             });
//           }
//         }
//       }
//       this.setState({ folderSticker: folderSticker });
//       Hooks.consoleLog(TAG + "folderSticker: ", this.state.folderSticker);

//     }).catch(e => {
//       Hooks.consoleLog(TAG + "error fsReadAssets: ", e);
//     });
//   }

//   sendSticker(payload) {
//     RNWhatsAppStickers.isWhatsAppAvailable().then(isWhatsAppAvailable => {
//       if (isWhatsAppAvailable) {
//         if (Platform.OS === 'ios') {
//           return undefined; // temporary
//         }

//         // ANDROID
//         return RNWhatsAppStickers.send(payload.identifier, payload.name).then(() => {
//           Hooks.consoleLog(TAG, 'success');
//         }).catch(e => {
//           Hooks.consoleLog(TAG + "error send sticker Android: ", e);
//         });
//       }

//       return undefined;
//     }).catch(e => {
//       Hooks.consoleLog(TAG + "error check WA available: ", e);
//     });
//   }

//   navToDetailStickers(stickerDetails, title) {
//     Hooks.consoleLog(TAG + "stickerDetails: ", stickerDetails);

//     this.setState({
//       modalShow: true,
//       modalTitle: title,
//       stickerDetails: stickerDetails
//     });
//   }

//   renderStickersPackageList() {
//     let component =
//       <FlatList
//         contentContainerStyle={[Styles.Helpers.mainCenter]}
//         data={this.state.folderSticker}
//         horizontal={false}
//         keyExtractor={(item, index) => "STICKER_PACKAGE_LIST_" + index}
//         refreshing={this.state.refreshing}
//         // onRefresh={() => this._onRefresh()}
//         // onEndReachedThreshold={0.5}
//         // onEndReached={() => this.loadMoreNotifications()}
//         ListEmptyComponent={() => {
//           return (
//             <View>
//               {
//                 !this.state.isLoading &&
//                 <View style={[
//                   Styles.Helpers.center,
//                   Styles.Helpers.fill,
//                 ]}>
//                   <Text style={{
//                     textAlign: 'center',
//                     fontWeight: 'bold',
//                     fontSize: 14,
//                     paddingVertical: 20
//                   }}>
//                     {lang('label.no_data')}
//                   </Text>
//                 </View>
//               }
//             </View>
//           );
//         }}
//         renderItem={
//           ({ item, index }) => {
//             return (
//               <TouchableOpacity
//                 onPress={() => this.navToDetailStickers(item, item.name)}
//                 key={"stickerPackageList_" + item.identifier}
//                 style={[
//                   Styles.Helpers.colMain,
//                   Styles.Helpers.mainStart,
//                   {
//                     marginHorizontal: 10,
//                     paddingVertical: 20,
//                     borderBottomColor: '#E5E5E5',
//                     borderBottomWidth: 1,
//                   }
//                 ]}
//               >
//                 <View style={[
//                   Styles.Helpers.fillCol,
//                   Styles.Helpers.row,
//                   Styles.Helpers.crossCenter,
//                 ]}>
//                   <Text style={[{
//                     fontFamily: 'Roboto-Medium',
//                     fontSize: 13,
//                     paddingRight: 20,
//                   }]}>
//                     {lang(item.name)}
//                   </Text>
//                   <Text
//                     numberOfLines={1}
//                     ellipsizeMode={'tail'}
//                     style={[{
//                       fontFamily: 'Roboto-Light',
//                       fontSize: 11,
//                     }]}
//                   >
//                     {lang(item.publisher)}
//                   </Text>
//                 </View>

//                 {this.renderPreviewStickers(item.identifier, item.stickers)}
//               </TouchableOpacity>
//             );
//           }
//         }
//       />
//       ;

//     return component;
//   }

//   renderPreviewStickers(identifier, array) {
//     let stickerPreview = [];

//     for (const [index, img] of array.entries()) {
//       if (index < 5) {
//         stickerPreview.push(
//           <Image
//             key={identifier + '/' + img.image_file}
//             fadeDuration={0}
//             source={{ uri: 'asset:/' + identifier + '/' + img.image_file }}
//             style={[{
//               marginTop: 10,
//               marginHorizontal: 5,
//               width: 50,
//               height: 50,
//             }]}
//           />
//         );
//       }
//     }

//     return (
//       <View style={[
//         Styles.Helpers.row,
//         Styles.Helpers.mainSpaceBetween,
//         Styles.Helpers.crossCenter,
//       ]}>
//         <View style={[
//           Styles.Helpers.row,
//           Styles.Helpers.center,
//         ]}>
//           {stickerPreview}
//         </View>
//         {/* <Icon
//           name="plus"
//           type="Entypo"
//           style={{
//             color: Styles.Colors.black,
//             fontSize: 30,
//             paddingRight: 20,
//           }}
//         /> */}
//       </View>
//     );
//   }

//   renderModalStickerDetails() {
//     let stickerData = this.state.stickerDetails;
//     let component =
//       <Modal
//         transparent={true}
//         animationType={'slide'}
//         visible={this.state.modalShow}
//         onRequestClose={() => this.setState({
//           modalShow: false,
//           modalTitle: '',
//           stickerDetails: []
//         })}
//       >
//         <AppFrame
//           headerLeft={
//             <Button
//               transparent
//               onPress={() => this.setState({
//                 modalShow: false,
//                 modalTitle: '',
//                 stickerDetails: []
//               })}
//             >
//               <Icon
//                 name="chevron-left"
//                 type="Octicons"
//                 style={{
//                   color: Styles.Colors.white,
//                   fontSize: 30,
//                   paddingRight: 20,
//                 }}
//               />
//             </Button>
//           }
//           headerRight={
//             <View />
//           }
//           // headerTitle={this.state.modalTitle}
//           headerTitle={'Airmas WA Stickers'}
//           headerTitleStyle={{
//             color: Styles.Colors.black
//           }}
//           renderContent={
//             <View style={[
//               Styles.Helpers.fillCol,
//               Styles.Helpers.mainCenter,
//             ]}>
//               <View style={[
//                 Styles.Helpers.row,
//                 Styles.Helpers.center,
//                 {
//                   paddingVertical: 10,
//                 }
//               ]}>
//                 <View style={[{
//                   flex: 0.3
//                 }]}>
//                   <Image
//                     fadeDuration={0}
//                     source={{
//                       uri: 'asset:/' +
//                         stickerData.identifier +
//                         '/' + stickerData.tray_image_file
//                     }}
//                     style={[Styles.Helpers.xxlImgBox]}
//                   />
//                 </View>
//                 <View style={[{
//                   flex: 0.7,
//                 }]}>
//                   <Text style={[
//                     {
//                       fontWeight: 'bold',
//                       fontSize: 20,
//                     }
//                   ]}>{lang(stickerData.name)}</Text>
//                   <Text style={[]}>{lang(stickerData.publisher)}</Text>
//                   <Text style={[]}>{stickerData.stickers.length + " sticker(s)"}</Text>
//                 </View>
//               </View>
//               {this.renderFileGrid(stickerData)}

//               {/* Button Send to Whatsapp */}
//               <TouchableOpacity
//                 activeOpacity={0.5}
//                 style={[
//                   Styles.Helpers.center,
//                   Styles.Helpers.row,
//                   {
//                     backgroundColor: 'green',
//                     borderRadius: 5,
//                     height: 50,
//                     marginHorizontal: 30,
//                     marginTop: 10,
//                     marginBottom: 20
//                   }
//                 ]}
//                 onPress={() => { this.sendSticker(stickerData) }}
//               >
//                 <Icon
//                   name="whatsapp"
//                   type="MaterialCommunityIcons"
//                   style={{
//                     color: Styles.Colors.white,
//                     fontSize: 30,
//                     paddingRight: 20,
//                   }}
//                 />
//                 <Text
//                   allowFontScaling={false}
//                   style={[
//                     {
//                       color: Styles.Colors.white,
//                       fontWeight: 'bold',
//                       textTransform: 'uppercase'
//                     }
//                   ]}
//                 >
//                   {lang('Send to Whatsapp')}
//                 </Text>
//               </TouchableOpacity>
//             </View>
//           }
//         />
//       </Modal>
//       ;

//     return component;
//   }

//   renderFileGrid(data, numColumns = 4, padding = 20) {
//     return (
//       <FlatList
//         contentContainerStyle={{
//           justifyContent: 'flex-start',
//           paddingBottom: 20,
//           flex: 1,
//         }}
//         data={data.stickers}
//         numColumns={numColumns}
//         horizontal={false}
//         keyExtractor={(item, index) => "FileGrid_" + index}
//         ListEmptyComponent={() => {
//           return (<View />);
//         }}
//         renderItem={
//           ({ item, index }) => {
//             return (
//               <View
//                 style={[
//                   Styles.Helpers.center, {
//                     aspectRatio: 1,
//                     padding: padding,
//                   }
//                 ]}
//               >
//                 <Image
//                   source={{ uri: 'asset:/' + data.identifier + '/' + item.image_file }}
//                   style={{
//                     // backgroundColor: ['yellow', 'gray', 'blue'][Math.floor(Math.random() * ['yellow', 'gray', 'blue'].length)],
//                     alignSelf: 'center',
//                     padding: 20,
//                     resizeMode: 'contain',
//                     aspectRatio: 1,
//                     width: (width / numColumns) - (padding * 2),
//                   }}
//                 />
//               </View>
//             );
//           }
//         }
//       />
//     );
//   }

//   render() {
//     return (
//       <AppFrame
//         headerTitle={'Airmas WA Stickers'}
//         renderContent={
//           <View style={[
//             Styles.Helpers.fill
//           ]}>
//             {
//               !this.state.isLoading ?
//                 this.renderStickersPackageList() :
//                 <Spinner
//                   style={{
//                     position: 'absolute',
//                     top: height * 0.15,
//                     left: 0,
//                     right: 0,
//                   }}
//                   color={Styles.Colors.black}
//                 />
//             }
//             {
//               this.state.stickerDetails.length != 0 &&
//               this.renderModalStickerDetails()
//             }
//           </View>
//         }
//       />
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: Styles.Colors.white,
//   },
//   welcome: {
//     fontSize: 20,
//     textAlign: 'center',
//     margin: 10,
//   },
//   instructions: {
//     textAlign: 'center',
//     color: Styles.Colors.black,
//     marginBottom: 5,
//   },
// });

// const mapStateToProps = state => ({
//   counter: state.counter,
//   refresh_app: state.refresh_app
// });

// const mapDispatchToProps = dispatch => bindActionCreators(AppActions, dispatch);

// export default connect(mapStateToProps)(Home);
