import React, { Component } from 'react';
import {
  Alert,
  Dimensions,
  FlatList,
  Image,
  Modal,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import { Button, Icon, Text, Spinner } from 'native-base';

import { connect } from 'react-redux';

import lang from 'App/Helpers/Languages';
import AppFrame from 'App/Components/AppFrame';
import Loader from 'App/Components/Loader';

import * as Styles from 'App/Styles';
import * as Hooks from 'App/Helpers/Hooks';

import RNWhatsAppStickersShare, { StickerPackConfig } from "react-native-whatsapp-stickers-share";

const TAG = 'HOME_v2: ';
const { width, height } = Dimensions.get('window');

const stickerPacks = [
  {
    identifier: 'roboroxsticker',
    title: 'Roborox Sticker',
    author: 'Roborox',
    trayImage: "https://api.sticker.place/v2/images/5dc96428e4b0e670e6ec2d34",
    publisherEmail: "vwpo@roborox.org",
    publisherURL: "https://roborox.org",
    privacyPolicyURL: "https://roborox.org/privacy",
    licenseURL: "https://roborox.org/license",
    stickers: [
      {
        url: "https://api.sticker.place/v2/images/5dc96428e4b0e670e6ec2d3a",
      },
      {
        url: "https://api.sticker.place/v2/images/5dc96428e4b0e670e6ec2d38",
      },
      {
        url: "https://api.sticker.place/v2/images/5dc96428e4b0e670e6ec2d34",
      },
      {
        url: "https://api.sticker.place/v2/images/5dc96429e4b0e670e6ec2d3c",
      }
    ]
  },
  {
    identifier: 'roboroxsticker2',
    title: 'Roborox Sticker v2',
    author: 'Roborox',
    trayImage: "https://api.sticker.place/v2/images/5dc96428e4b0e670e6ec2d34",
    publisherEmail: "vwpo@roborox.org",
    publisherURL: "https://roborox.org",
    privacyPolicyURL: "https://roborox.org/privacy",
    licenseURL: "https://roborox.org/license",
    stickers: [
      {
        url: "https://api.sticker.place/v2/images/5dc96428e4b0e670e6ec2d38",
      },
      {
        url: "https://api.sticker.place/v2/images/5dc96428e4b0e670e6ec2d3a",
      },
      {
        url: "https://api.sticker.place/v2/images/5dc96429e4b0e670e6ec2d3c",
      },
      {
        url: "https://api.sticker.place/v2/images/5dc96428e4b0e670e6ec2d34",
      },
    ]
  },
  {
    identifier: 'roboroxsticker3',
    title: 'Roborox Sticker v3',
    author: 'Roborox',
    trayImage: "https://api.sticker.place/v2/images/5dc96428e4b0e670e6ec2d34",
    publisherEmail: "vwpo@roborox.org",
    publisherURL: "https://roborox.org",
    privacyPolicyURL: "https://roborox.org/privacy",
    licenseURL: "https://roborox.org/license",
    stickers: [
      {
        url: "https://api.sticker.place/v2/images/5dc96428e4b0e670e6ec2d34",
      },
      {
        url: "https://api.sticker.place/v2/images/5dc96428e4b0e670e6ec2d3a",
      },
      {
        url: "https://api.sticker.place/v2/images/5dc96428e4b0e670e6ec2d38",
      },
      {
        url: "https://api.sticker.place/v2/images/5dc96429e4b0e670e6ec2d3c",
      },
    ]
  }
];

class Home extends Component {
  static navigationOptions = () => ({ headerShown: false });

  constructor(props) {
    super(props);

    this.state = {
      folderSticker: [],
      stickerDetails: [],
      modalTitle: '',
      modalShow: false,
      isLoading: true,
      applyStickerLoading: false,
      refreshing: false,
    }
  }

  componentDidMount() {
    this.setState({
      folderSticker: stickerPacks,
      isLoading: false
    });
  }

  sendSticker_v2(stickerConfig: StickerPackConfig) {
    Hooks.consoleLog(TAG + "stickerConfig: ", stickerConfig);

    this.setState({ applyStickerLoading: true });

    RNWhatsAppStickersShare.isWhatsAppAvailable().then(isWhatsAppAvailable => {
      if (isWhatsAppAvailable) {
        return RNWhatsAppStickersShare.share(stickerConfig).then(res => {
          Hooks.consoleLog(TAG + "sendSticker_v2: ", res);
          this.setState({ applyStickerLoading: false });
        }).catch(e => {
          Hooks.consoleLog(TAG + 'error sendSticker_v2 Android: ', e);
          this.setState({ applyStickerLoading: false });
        });
      }
      this.setState({ applyStickerLoading: false });
      return Alert.alert("You should install WhatsApp first");
    }).catch(err => {
      this.setState({ applyStickerLoading: false });
      return Alert.alert("An error occured", err);
    });
  }

  navToDetailStickers(stickerDetails, title) {
    Hooks.consoleLog(TAG + 'stickerDetails: ', stickerDetails);

    this.setState({
      modalShow: true,
      modalTitle: title,
      stickerDetails: stickerDetails
    });
  }

  renderStickersPackageList_v2() {
    let component =
      <FlatList
        contentContainerStyle={[Styles.Helpers.mainCenter]}
        data={this.state.folderSticker}
        horizontal={false}
        keyExtractor={(item, index) => 'STICKER_PACKAGE_LIST_' + index}
        refreshing={this.state.refreshing}
        // onRefresh={() => this._onRefresh()}
        // onEndReachedThreshold={0.5}
        // onEndReached={() => this.loadMoreNotifications()}
        ListEmptyComponent={() => {
          return (
            <View>
              {
                !this.state.isLoading &&
                <View style={[
                  Styles.Helpers.center,
                  Styles.Helpers.fill,
                ]}>
                  <Text style={{
                    textAlign: 'center',
                    fontWeight: 'bold',
                    fontSize: 14,
                    paddingVertical: 20
                  }}>
                    {lang('label.no_data')}
                  </Text>
                </View>
              }
            </View>
          );
        }}
        renderItem={
          ({ item, index }) => {
            return (
              <View style={[
                Styles.Helpers.row,
                Styles.Helpers.crossCenter,
                Styles.Helpers.mainSpaceBetween,
                {
                  marginHorizontal: 10,
                  paddingVertical: 20,
                  borderBottomColor: '#E5E5E5',
                  borderBottomWidth: 1,
                }
              ]}>
                <TouchableOpacity
                  onPress={() => this.navToDetailStickers(item, item.title)}
                  key={'stickerPackageList_' + item.identifier}
                  style={[
                    Styles.Helpers.colMain,
                    Styles.Helpers.mainStart,
                  ]}
                >
                  <View style={[
                    Styles.Helpers.fillCol,
                    Styles.Helpers.row,
                    Styles.Helpers.crossCenter,
                  ]}>
                    <Text style={[{
                      fontFamily: 'Roboto-Medium',
                      fontSize: 16,
                      fontWeight: 'bold',
                      paddingRight: 20,
                    }]}>
                      {lang(item.title)}
                    </Text>
                    <Text
                      numberOfLines={1}
                      ellipsizeMode={'tail'}
                      style={[{
                        fontFamily: 'Roboto-Light',
                        fontSize: 12,
                      }]}
                    >
                      {lang(item.author)}
                    </Text>
                  </View>

                  {this.renderPreviewStickers_v2(item.identifier, item.stickers)}
                </TouchableOpacity>

                <TouchableOpacity
                  activeOpacity={0.5}
                  onPress={() => this.sendSticker_v2(item)}
                >
                  <Icon
                    name='plus'
                    type='Entypo'
                    style={{
                      paddingTop: 30,
                      color: Styles.Colors.primary,
                      fontSize: 30,
                      paddingRight: 20,
                    }}
                  />
                </TouchableOpacity>
              </View>
            );
          }
        }
      />
      ;

    return component;
  }

  renderPreviewStickers_v2(identifier, array) {
    let stickerPreview = [];

    for (const [index, img] of array.entries()) {
      if (index < 5) {
        stickerPreview.push(
          <Image
            key={img.url}
            fadeDuration={0}
            source={{ uri: img.url }}
            style={[{
              marginTop: 10,
              marginHorizontal: 5,
              width: 50,
              height: 50,
            }]}
          />
        );
      }
    }

    return (
      <View style={[
        Styles.Helpers.row,
        Styles.Helpers.mainSpaceBetween,
        Styles.Helpers.crossCenter,
      ]}>
        <View style={[
          Styles.Helpers.row,
          Styles.Helpers.center,
        ]}>
          {stickerPreview}
        </View>
        {/* <Icon
          name='plus'
          type='Entypo'
          style={{
            color: Styles.Colors.black,
            fontSize: 30,
            paddingRight: 20,
          }}
        /> */}
      </View>
    );
  }

  renderModalStickerDetails() {
    let stickerData = this.state.stickerDetails;
    let component =
      <Modal
        transparent={true}
        animationType={'slide'}
        visible={this.state.modalShow}
        onRequestClose={() => this.setState({
          modalShow: false,
          modalTitle: '',
          stickerDetails: []
        })}
      >
        <AppFrame
          headerLeft={
            <Button
              transparent
              onPress={() => this.setState({
                modalShow: false,
                modalTitle: '',
                stickerDetails: []
              })}
            >
              <Icon
                name='chevron-left'
                type='Octicons'
                style={{
                  color: Styles.Colors.white,
                  fontSize: 30,
                  paddingRight: 20,
                }}
              />
            </Button>
          }
          headerRight={
            <View />
          }
          // headerTitle={this.state.modalTitle}
          headerTitle={'WA Stickers'}
          headerTitleStyle={{
            color: Styles.Colors.black
          }}
          renderContent={
            <View style={[
              Styles.Helpers.fillCol,
              Styles.Helpers.mainCenter,
            ]}>
              <View style={[
                Styles.Helpers.row,
                Styles.Helpers.center,
                {
                  paddingVertical: 10,
                }
              ]}>
                <View style={[{
                  flex: 0.3
                }]}>
                  <Image
                    fadeDuration={0}
                    source={{ uri: stickerData.trayImage }}
                    style={[Styles.Helpers.xxlImgBox]}
                  />
                </View>
                <View style={[{
                  flex: 0.7,
                }]}>
                  <Text style={[
                    {
                      fontWeight: 'bold',
                      fontSize: 20,
                    }
                  ]}>{lang(stickerData.title)}</Text>
                  <Text style={[]}>{lang(stickerData.author)}</Text>
                  <Text style={[]}>{stickerData.stickers.length + ' sticker(s)'}</Text>
                </View>
              </View>
              {this.renderFileGrid(stickerData)}

              {/* Button Send to Whatsapp */}
              <TouchableOpacity
                activeOpacity={0.5}
                style={[
                  Styles.Helpers.center,
                  Styles.Helpers.row,
                  {
                    backgroundColor: 'green',
                    borderRadius: 5,
                    height: 50,
                    marginHorizontal: 30,
                    marginTop: 10,
                    marginBottom: 20
                  }
                ]}
                onPress={() => this.sendSticker_v2(stickerData)}
              >
                <Icon
                  name='whatsapp'
                  type='MaterialCommunityIcons'
                  style={{
                    color: Styles.Colors.white,
                    fontSize: 30,
                    paddingRight: 20,
                  }}
                />
                <Text
                  allowFontScaling={false}
                  style={[
                    {
                      color: Styles.Colors.white,
                      fontWeight: 'bold',
                      textTransform: 'uppercase'
                    }
                  ]}
                >
                  {lang('Send to Whatsapp')}
                </Text>
              </TouchableOpacity>
            </View>
          }
        />
      </Modal>
      ;

    return component;
  }

  renderFileGrid(data, numColumns = 4, padding = 20) {
    return (
      <FlatList
        contentContainerStyle={{
          justifyContent: 'flex-start',
          paddingBottom: 20,
          flex: 1,
        }}
        data={data.stickers}
        numColumns={numColumns}
        horizontal={false}
        keyExtractor={(item, index) => 'FileGrid_' + index}
        ListEmptyComponent={() => {
          return (<View />);
        }}
        renderItem={
          ({ item, index }) => {
            return (
              <View
                style={[
                  Styles.Helpers.center, {
                    aspectRatio: 1,
                    padding: padding,
                  }
                ]}
              >
                <Image
                  source={{ uri: item.url }}
                  style={{
                    // backgroundColor: ['yellow', 'gray', 'blue'][Math.floor(Math.random() * ['yellow', 'gray', 'blue'].length)],
                    alignSelf: 'center',
                    padding: 20,
                    resizeMode: 'contain',
                    aspectRatio: 1,
                    width: (width / numColumns) - (padding * 2),
                  }}
                />
              </View>
            );
          }
        }
      />
    );
  }

  render() {
    return (
      <AppFrame
        headerTitle={'WA Stickers'}
        renderContent={
          <View style={[
            Styles.Helpers.fill
          ]}>
            {
              !this.state.isLoading ?
                this.renderStickersPackageList_v2() :
                <Spinner
                  style={{
                    position: 'absolute',
                    top: height * 0.15,
                    left: 0,
                    right: 0,
                  }}
                  color={Styles.Colors.black}
                />
            }
            {
              this.state.stickerDetails.length != 0 &&
              this.renderModalStickerDetails()
            }
            <Loader loading={this.state.applyStickerLoading} />
          </View>
        }
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Styles.Colors.white,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: Styles.Colors.black,
    marginBottom: 5,
  },
});

const mapStateToProps = state => ({
  counter: state.counter,
  refresh_app: state.refresh_app
});

const mapDispatchToProps = dispatch => bindActionCreators(AppActions, dispatch);

export default connect(mapStateToProps)(Home);
